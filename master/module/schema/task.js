const mongoose = require("mongoose")
const Schema = mongoose.Schema;

const taskSchema = new Schema({
	task: {
		type: String
	},
	pid: {
		type: String
	},
	status: {
		type: String
	},
	timestamp:{
		type: Date,
		default: Date.now()
	}
})
module.exports = mongoose.model("task", taskSchema);