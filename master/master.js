require("dotenv").config()
const express = require("express")
const app = express()
const cors= require("cors")
const dbConfig = require('./config/db');
require("./module/feedback_handlers") 

// for verbose logging-
app.use(require("morgan")(process.env.logenv))
const bodyParser = require("body-parser")
app.use(bodyParser.json())
app.use(cors())
app.use(bodyParser.urlencoded({extended:false}))


// connect to mongoDB
dbConfig.connect();

//endpoints
app.use(express.static("views"))

app.use(require("./routes/index"))


app.use((err,req,res,next)=>{
	console.error(err)
	res.send({err:err.message})
})
app.listen(process.env.port || 3000,()=>console.log("listening..."))
module.exports=app
