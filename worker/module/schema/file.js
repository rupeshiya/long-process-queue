const mongoose = require("mongoose")
const Schema = mongoose.Schema;

const fileSchema = new Schema({
	date:{
		type:Date,
		default:Date.now()
	},
	taskId:{
		type:String
	},
	processId: {
		type: String
	},
	o: {
		type: Object
	}
})

module.exports = mongoose.model('file', fileSchema);