const mongoose = require('mongoose');
const mongoURL = process.env.MONGO_WORKER_URL || "mongodb://localhost:27017/worker";

module.exports.connect = async () => {
  mongoose.connect(`${mongoURL}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    retryWrites: false
  })
  .then(() => {
    console.log('Mongo connected!');
  })
  .catch((err) => {
    console.log(err);
  })
}
