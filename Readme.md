# Long running process handling

Atlan is a data democratization company that helps teams collaborate frictionlessly on data projects.This is a internship task for a Backend Developer Internship where the task given is to build a solution for providing large scale concurrency in long running tasks. In this case A long running task is a csv file that consists of 1-2 million records. These records have to parsed and indexed into a Database concurrently. The solution provided should be able to terminate running tasks and restart the task.

## Features

- [x] Start tasks  (add csv data to db)
- [x] Web dashboard for monitoring running tasks
- [x] NodeJS Multi process for reducing downtime


## Tech used: 

- MongoDB - used for maintaining state of tasks

- BULL - A Task Queue built on top of redis

- REDIS - A broker for BULL

- NodeJS BULL Processes - A Pool of worker processes work on a long running task.

## Requirements
    
    Node.js, Express.js, Mongodb, Redis, Bull 

## Installation Steps

```
    1. Change director to /master and run master process using **npm run master**
    2. Change directory to /worker and run worker process using **npm run worker**
    3. Move to localhost:3000 to check web view 
```

### APIs

##### Start task
```
    GET http://localhost:5000/start
```

##### Stop task
```
    POST http://localhost:5000/stop 
    Body: {
        id: "taskId"
    }
```

##### Get all running tasks
```
    GET http://localhost:5000/tasks
```

#### Requirements:

```
    PORT=5000
    NODE_ENV="development"
    JWT_SECRET="thisismysupersecrettokenjustkidding"
    MONGO_MASTER_URL="mongodb://<user>:<pass>@ds135876.mlab.com:35876/master"
    MONGO_WORKER_URL="mongodb://<user>:<pass>@ds135876.mlab.com:35876/worker"
    REDIS_HOST="redis"
    REDIS_PORT=6379
    REDIS_PASSWORD="auth"
    REDIS_DB=0
    REDIS_ACTIVITY_DB=1
```